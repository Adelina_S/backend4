<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();
  $errors_value = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  
  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['yob'] = !empty($_COOKIE['yob_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['sp'] = !empty($_COOKIE['sp_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);

  // Выдаем сообщения об ошибках.
  if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    $errors_value['name'] = empty($_COOKIE['name_error_value']) ? '' : $_COOKIE['name_error_value'];
    setcookie('name_error_value', '', 100000);
    $messages[] = '<div class="error">Подкорректируйте имя. При заполнении поля используйте только символы киррилицы либо латиницы.</div>';
  }
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $errors_value['email'] = empty($_COOKIE['email_error_value']) ? '' : $_COOKIE['email_error_value'];
    setcookie('email_error_value', '', 100000);
    $messages[] = '<div class="error">Подкорректируйте email. Убедитесь, что поле заполнено в соответствии с шаблоном.</div>';
  }
  if ($errors['yob']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('yob_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберете год рождения.</div>';
  }
  if ($errors['sex']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберете пол.</div>';
  }
  if ($errors['sp']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sp_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберете суперспособность(-ти).</div>';
  }
  if ($errors['limbs']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('limbs_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Укажите количество конечностей.</div>';
  }
  if ($errors['checkbox']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('checkbox_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Ознакомьтесь с контрактом.</div>';
  }
  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['yob'] = empty($_COOKIE['yob_value']) ? '' : $_COOKIE['yob_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['sp'] = empty($_COOKIE['sp_value']) ? '' : $_COOKIE['sp_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];

  // Включаем содержимое файла index.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

else{
// Проверяем ошибки.
$errors = FALSE;
$pattern = '/^([А-Я]{1}[а-яё]{1,23}|[A-Z]{1}[a-z]{1,23})$/u';
if (preg_match($pattern, $_POST['name'])) {
 // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('name_value', $_POST['name'], time() + 12 * 30 * 24 * 60 * 60); 
}
else {
 // Выдаем куку на день с флажком об ошибке в поле name.
  setcookie('name_error_value', $_POST['name'], time() + 24 * 60 * 60);
  setcookie('name_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
//\w+@[a-z|а-я]+\.[a-z|а-я]+
$pattern = '/\w+@[a-z]+\.[a-z]+/';
if (preg_match($pattern, $_POST['email'])) {
 setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
}
else {
  setcookie('email_error_value', $_POST['email'], time() + 24 * 60 * 60);
 setcookie('email_error', '1', time() + 24 * 60 * 60);
 $errors = TRUE;
}

if (empty($_POST['sex'])) {
  setcookie('sex_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['yob'])) {
  setcookie('yob_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('yob_value', $_POST['yob'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['limbs'])) {
  setcookie('limbs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['superpowers'])) {
  setcookie('sp_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('sp_value', $_POST['superpowers'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['limbs'])) {
  // Выдаем куку на день с флажком об ошибке в поле name.
  setcookie('limbs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['checkbox'])) {
  // Выдаем куку на день с флажком об ошибке в поле name.
  setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}

if ($errors) {
  // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
  header('Location: index.php');
  exit();
}
else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('yob_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('sp_error', '', 100000);
    setcookie('limbs_error', '', 100000);
}

// Сохранение в базу данных.

$user = 'u16306';
$pass = '2435324';
$db = new PDO('mysql:host=localhost;dbname=u16306', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$name = $_POST['name'];
$email = $_POST['email'];
$year_of_birth = $_POST['yob'];
$gender = $_POST['sex'];
$num_of_limbs = $_POST['limbs'];
$sp = $_POST['superpowers'];
$biography = $_POST['biography'];


// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO `user` SET name = ?, email = ?, year_of_birth = ?, gender = ?, num_of_limbs = ?, biography = ?");
  $stmt -> execute(array($name, $email, $year_of_birth, $gender, $num_of_limbs, $biography));
  $stmt = $db->prepare("SELECT `id` FROM `user` WHERE name = ? AND email = ?");
  $stmt->execute(array($name, $email));
  $result_user = $stmt->fetchAll();
  $sp_id = array();
  $result_sp_map;
  for($i=0; $i<count($sp); $i++)
  {
    $stmt = $db->prepare("SELECT `sp_id` FROM `super_power_map` WHERE sp_name = ?");
    $stmt ->execute(array($sp[$i]));
    $sp_id[$i] = $stmt->fetchColumn();
  }
  
  for($i=0; $i<count($sp); $i++)
  {
    $stmt = $db->prepare("INSERT INTO `super_power` SET id = ?, sp_id = ?");
    $stmt ->execute(array($result_user[0]["id"], $sp_id[$i]));
  }
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

// Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
